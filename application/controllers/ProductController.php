<?php
defined('BASEPATH') or exit('No direct script access allowed');

class ProductController extends CI_Controller
{
	public function __construct()
	{
		parent::__construct();
		$this->load->model('Product', 'ProductModel');
	}

	public function getDetail()
	{
		$url = $this->input->get('url', null);
		$name = explode('/', $url)[3] ?? 'Unknown-name';
		$matchesAsins = [];
		
		preg_match("/dp\/(.{10})/i", $url, $matchesAsins);
		if (empty($matchesAsins[1])) preg_match("/gp\/product\/(.{10})/i", $url, $matchesAsins);
		$asin = $matchesAsins[1] ?? null;

		$product = null;
		if ($asin && $url) {
			$conditions = ['asin' => $asin];
			$conditions['url'] = $url;
			$product = $this->ProductModel->getDetail('amz', $conditions);
		}

		return $this->load->view('product_detail', ['product' => $product ? json_encode($product) : null]);
	}

	public function getVariantDetail($asin)
	{
		$res = [
			'success'	=> false,
			'errors'	=> []
		];

		$conditions = ['asin' => $asin];
		$conditions['url'] = "amazon.com/dp/$asin";
		$res['data'] = $this->ProductModel->getDetail('amz', $conditions);

		if (!empty($res['data'])) {
			$res['success'] = true;
			unset($res['variants']);
		}

		die(json_encode($res));
	}

	public function getVariants($asin)
	{
		$res = [
			'success'	=> false,
			'errors'	=> '',
			'data'		=> []
		];

		// get product detail by ajax
		$conditions = ['asin' => $asin];
		$variants = $this->ProductModel->getVariants('amz', $conditions);

		$res['success'] = !empty($variants) ? true : false;
		$res['data'] = $variants;
		die(json_encode($res));
	}
}
