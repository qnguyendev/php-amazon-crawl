<?php
defined('BASEPATH') or exit('No direct script access allowed');

use GuzzleHttp\Client;
use GuzzleHttp\Psr7\Request;

class SuperAmzProductApi extends CI_Model
{
	public function __construct()
	{
		parent::__construct();
		$this->load->helper('date');
	}

	public function getDetail($conditions = [])
	{
		// call api get variants
		$this->checkAndCrawlVariants($conditions['asin']);
		$crawledData = $this->crawl($conditions);

		// debug
		// $this->db->insert('amz_product_samples', [
		// 	'asin'	=> $conditions['asin'],
		// 	'params' => $crawledData,
		// 	'src'	=> 'super-amz-api'
		// ]);

		$parsedData = $this->parseFromCrawl($crawledData);
		if (!$parsedData) return;

		// in dev mode
		// $parsedData = (array) $this->db->where('asin', $conditions['asin'])->get('amz_products')->first_row();

		// save new detail every crawl
		try {
			$parsedData['asin'] = $conditions['asin'];
			$existProduct = $this->db->where('asin', $parsedData['asin'])->get('amz_products')->first_row();

			if ($existProduct)
				$this->db->where('asin', $parsedData['asin'])->update('amz_products', $parsedData);
			else
				$this->db->insert('amz_products', $parsedData);

			// show variant variants 
			$parsedData['variants'] = [];

			return $parsedData;
		} catch (\Throwable $th) {
			$this->db->insert('error_logs', [
				'time'	=> mdate('%Y-%m-%d %h:%i', now('asia/bangkok')),
				'error'	=> $th,
				'type'	=> 'update-from-crawl-proxyCrawl'
			]);
		}

		return;
	}

	public function getVariants($conditions = [])
	{
		$variants = [];
		$representGroupAsin = $this->db->where('asin', $conditions['asin'])
			->get('amz_product_groups')->first_row()->represent_asin ?? null;

		if ($representGroupAsin) {
			$variants = $this->db->select('ap.asin, ap.variant_name, ap.main_image')
				->from('amz_product_groups as apg')
				->join('amz_products as ap', 'ap.asin = apg.asin')
				->where('apg.represent_asin', $representGroupAsin)->get()->result();
		}
		return $variants;
	}

	public function checkAndCrawlVariants($asin)
	{
		try {
			$client = new Client(['timeout'  => 1.5]);
			$client->request('GET', 'localhost:3000/checkAndCrawl', [
				'query' => ['asin' => $asin]
			]);
		} catch (\Throwable $th) {
		}
	}

	public function crawl($conditions)
	{
		$curl = curl_init();

		curl_setopt_array($curl, [
			CURLOPT_URL => "https://rapidapi.p.rapidapi.com/product/{$conditions['asin']}",
			CURLOPT_RETURNTRANSFER => true,
			CURLOPT_FOLLOWLOCATION => true,
			CURLOPT_ENCODING => "",
			CURLOPT_MAXREDIRS => 10,
			CURLOPT_TIMEOUT => 30,
			CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
			CURLOPT_CUSTOMREQUEST => "GET",
			CURLOPT_HTTPHEADER => [
				"x-rapidapi-host: super-amazon-products.p.rapidapi.com",
				"x-rapidapi-key: a70b982d62msh691de99db244fc6p142d5bjsnd3d7f4ab6fa6" // qnguyen.dev
                // "x-rapidapi-key: 479d9f2f88msh9b07230f6d25980p1c754djsn062d7ce368fb" // qnguyen.play

			],
		]);

		$response = curl_exec($curl);
		$err = curl_error($curl);
		curl_close($curl);

		return !empty($err) ? $err : $response;
		// curl_close($curl);

		// if ($err) app
		// echo "cURL Error #:" . $err;
		

		// return $response;
	}

	public function parseFromCrawl($response)
	{
		// use for not exist in db
		$response = json_decode($response, true);
		if (empty($response) || empty($response['success']) || empty($response['result'])) return;

		$data = $response['result'];

		$parsedData = [
			'name' 			=> $data['title'],
			'price'			=> $data['price'] ?? $data['list_price'],
			'symbol'		=> '$',
			'in_stock'		=> $data['in_stock'] ? 1 : 0,
			'category'		=> !empty($data['category']) ?  json_encode($data['category']) : null,
			'main_image'	=> !empty($data['images']) ? $data['images'][0] ?? null : null,
			'images'		=> !empty($data['images']) ? json_encode($data['images']) : null,
			'features'		=> !empty($data['product_details']) ? json_encode($data['product_details']) : null,
			'description'	=> !empty($data['product_description']) && is_array($data['product_description']) ? $data['product_description'][0] : null,
			'time'			=> mdate('%Y-%m-%d %h:%i', now('asia/bangkok')),
			'weight'		=> 0
		];

		// get weight
		$matchesOunces = [];
		$matchesPounds = [];
		preg_match("/(\d+(\.\d{0,})?|\.?\d{1,}) ounces/i", strtolower($data['item_weight']), $matchesOunces);
		preg_match("/(\d+(\.\d{0,})?|\.?\d{1,}) pounds/i", strtolower($data['item_weight']), $matchesPounds);
		$parsedData['weight'] = !empty($matchesOunces[1]) ? $matchesOunces[1] * 0.0283 : $parsedData['weight'];
		$parsedData['weight'] = !empty($matchesPounds[1]) ? $matchesPounds[1] * 0.4535 : $parsedData['weight'];

		return $parsedData;
	}
}
