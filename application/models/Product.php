<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Product extends CI_Model {
	public function __construct() {
		parent::__construct();
		// $this->load->model('AmzProductProxyCrawlApi', 'AmzProductModel'); // api proxy crawl
		// $this->load->model('SuperAmzProductApi', 'AmzProductModel'); // api super amz product
		$this->load->model('AxessoAmzApi', 'AmzProductModel'); // api super amz product
	}

	public function getDetail($type = '', $conditions = []) {
		$product = null;
		switch ($type) {
			case 'amz':
				// get product by amzProduct model
				$product = $this->AmzProductModel->getDetail($conditions) ?? null;
				break;
		}
		// get product from db by type
		
		return $product;
	}

	// public function getVariants($type = '', $conditions) {
	// 	$variants = null;
	// 	switch ($type) {
	// 		case 'amz':
	// 			// get product by amzProduct model
	// 			$variants = $this->AmzProductModel->getVariants($conditions) ?? null;
	// 			break;
	// 	}
	// 	// get product from db by type
		
	// 	return $variants;
	// }
}
