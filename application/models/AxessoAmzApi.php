<?php
defined('BASEPATH') or exit('No direct script access allowed');

use GuzzleHttp\Client;
use GuzzleHttp\Psr7\Request;

class AxessoAmzApi extends CI_Model
{
	public function __construct()
	{
		parent::__construct();
		$this->load->helper('date');
	}

	public function getDetail($conditions = [])
	{
		$existProduct = $this->db->where('asin', $conditions['asin'])->get('amz_products')->first_row();

		if (!$existProduct) {
			$crawledData = $this->crawl($conditions);
			$parsedData = $this->parseFromCrawl($crawledData);

			if (!$parsedData) return;

			// save new detail every crawl
			try {
				$parsedData['asin'] = $conditions['asin'];
				$existProduct = $this->db->where('asin', $parsedData['asin'])->get('amz_products')->first_row();

				if ($existProduct)
					$this->db->where('asin', $parsedData['asin'])->update('amz_products', $parsedData);
				else
					$this->db->insert('amz_products', $parsedData);

				return $parsedData;
			} catch (\Throwable $th) {
				$this->db->insert('error_logs', [
					'time'	=> mdate('%Y-%m-%d %h:%i:%s', now('asia/bangkok')),
					'error'	=> $th,
					'type'	=> 'update-from-crawl-proxyCrawl'
				]);
			}
		} else {
			return (array)$existProduct;
		}

		return;
	}

	public function crawl($conditions)
	{

		$curl = curl_init();

		curl_setopt_array($curl, [
			CURLOPT_URL => "https://axesso-axesso-amazon-data-service-v1.p.rapidapi.com/amz/amazon-lookup-product?url=" . urlencode($conditions['url']),
			CURLOPT_RETURNTRANSFER => true,
			CURLOPT_FOLLOWLOCATION => true,
			CURLOPT_ENCODING => "",
			CURLOPT_MAXREDIRS => 10,
			CURLOPT_TIMEOUT => 30,
			CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
			CURLOPT_CUSTOMREQUEST => "GET",
			CURLOPT_HTTPHEADER => [
				"x-rapidapi-host: axesso-axesso-amazon-data-service-v1.p.rapidapi.com",
				"x-rapidapi-key: a70b982d62msh691de99db244fc6p142d5bjsnd3d7f4ab6fa6", // qnguyen.dev
				//"x-rapidapi-key: 479d9f2f88msh9b07230f6d25980p1c754djsn062d7ce368fb", // qnguyen.play
			],
		]);

		$response = curl_exec($curl);
		// $response = $this->getsample();
		$err = curl_error($curl);
		curl_close($curl);

		return !empty($err) ? $err : $response;
		// curl_close($curl);

		// if ($err) app
		// echo "cURL Error #:" . $err;


		// return $response;
	}

	public function getsample()
	{
		$json = file_get_contents('http://php-amazon-crawl.local/testdata.json');
		return $json;
	}

	public function parseFromCrawl($response)
	{
		// use for not exist in db
		$response = json_decode($response, true);
		//if (empty($response) || empty($response['success']) || empty($response['result'])) return;
		if (empty($response) || $response['responseStatus'] != 'PRODUCT_FOUND_RESPONSE') return;


		$parsedData = [
			'name' 				=> $response['productTitle'],
			'price'				=> $response['price'] > 0 ? $response['price'] : $response['retailPrice'],
			'sale_price'		=> $response['salePrice'] > 0 ? $response['salePrice'] : $response['dealPrice'],
			'shipping_price'	=> $response['shippingPrice'],
			'shipping_detail'	=> $response['priceShippingInformation'],
			'symbol'			=> $response['currency']['symbol'] ?? '$',
			'currency'			=> $response['currency']['code'],
			'in_stock'			=> $response['warehouseAvailability'],
			'category'			=> !empty($response['categories']) ?  json_encode($response['categories']) : null,
			'main_image'		=> !empty($response['mainImage']) ? $response['mainImage']['imageUrl'] ?? null : null,
			'images'			=> !empty($response['imageUrlList']) ? json_encode($response['imageUrlList']) : null,
			'features'			=> !empty($response['features']) ? json_encode($response['features']) : null,
			'description'		=> !empty($response['productDescription']) && is_array($response['productDescription']) ? $response['product_description'][0] : null,
			'time'				=> mdate('%Y-%m-%d %h:%i', now('asia/bangkok')),
			'weight'			=> 0,
			'variants'			=> !empty($response['variations']) ? json_encode($response['variations']) : null
		];

		try {
			if ($response['productDetails'])
				foreach ($response['productDetails'] as $info) {
					switch (trim(strtolower($info['name']))) {
						case 'package dimensions':
							$matchesOunces = [];
							$matchesPounds = [];
							preg_match("/(\d+(\.\d{0,})?|\.?\d{1,}) ounces/i", strtolower($info['value']), $matchesOunces);
							preg_match("/(\d+(\.\d{0,})?|\.?\d{1,}) pounds/i", strtolower($info['value']), $matchesPounds);
							$parsedData['weight'] = !empty($matchesOunces[1]) ? $matchesOunces[1] * 0.0283 : $parsedData['weight'];
							$parsedData['weight'] = !empty($matchesPounds[1]) ? $matchesPounds[1] * 0.4535 : $parsedData['weight'];
							break;

						case 'product dimensions':
							$matchesOunces = [];
							$matchesPounds = [];
							preg_match("/(\d+(\.\d{0,})?|\.?\d{1,}) ounces/i", strtolower($info['value']), $matchesOunces);
							preg_match("/(\d+(\.\d{0,})?|\.?\d{1,}) pounds/i", strtolower($info['value']), $matchesPounds);
							$parsedData['weight'] = !empty($matchesOunces[1]) ? $matchesOunces[1] * 0.0283 : $parsedData['weight'];
							$parsedData['weight'] = !empty($matchesPounds[1]) ? $matchesPounds[1] * 0.4535 : $parsedData['weight'];
							break;

						case 'item weight':
							$matchesOunces = [];
							$matchesPounds = [];
							preg_match("/(\d+(\.\d{0,})?|\.?\d{1,}) ounces/i", strtolower($info['value']), $matchesOunces);
							preg_match("/(\d+(\.\d{0,})?|\.?\d{1,}) pounds/i", strtolower($info['value']), $matchesPounds);
							$parsedData['weight'] = !empty($matchesOunces[1]) ? $matchesOunces[1] * 0.0283 : $parsedData['weight'];
							$parsedData['weight'] = !empty($matchesPounds[1]) ? $matchesPounds[1] * 0.4535 : $parsedData['weight'];
							break;

						case 'asin':
							$parsedData['asin'] = $info['value'];
							break;
					}
				}
		} catch (\Throwable $th) {
			//throw $th;
		}

		return $parsedData;
	}
}
