<?php
defined('BASEPATH') or exit('No direct script access allowed');

use GuzzleHttp\Client;
use GuzzleHttp\Psr7\Request;

class AmzProductProxyCrawlApi extends CI_Model
{
	public function __construct()
	{
		parent::__construct();
		$this->load->helper('date');
	}

	public function getDetail($conditions = [])
	{
		// call api get variants
		$this->checkAndCrawlProduct($conditions['asin']);

		$crawledData = $this->crawlByProxyCrawl($conditions);
		$parsedData = $this->parseProxyCrawl($crawledData);

		// in dev mode
		// $parsedData = (array) $this->db->where('asin', $conditions['asin'])->get('amz_products')->first_row();

		// save new detail every crawl
		try {
			$parsedData['asin'] = $conditions['asin'];
			$existProduct = $this->db->where('asin', $parsedData['asin'])->get('amz_products')->first_row();

			if ($existProduct)
				$this->db->where('asin', $parsedData['asin'])->update('amz_products', $parsedData);
			else
				$this->db->insert('amz_products', $parsedData);


			// show variant variants 
			$parsedData['variants'] = [];

			return $parsedData;
		} catch (\Throwable $th) {
			$this->db->insert('error_logs', [
				'time'	=> mdate('%Y-%m-%d %h:%i', now('asia/bangkok')),
				'error'	=> $th,
				'type'	=> 'update-from-crawl-proxyCrawl'
			]);
		}

		return;
	}

	public function getVariants($conditions = [])
	{
		$variants = [];
		$representGroupAsin = $this->db->where('asin', $conditions['asin'])
			->get('amz_product_groups')->first_row()->represent_asin ?? null;

		if ($representGroupAsin) {
			$variants = $this->db->select('ap.asin, ap.variant_name, ap.main_image')
				->from('amz_product_groups as apg')
				->join('amz_products as ap', 'ap.asin = apg.asin')
				->where('apg.represent_asin', $representGroupAsin)->get()->result();
		}
		return $variants;
	}

	public function checkAndCrawlProduct($asin)
	{
		try {
			$client = new Client(['timeout'  => 1.5]);
			$client->request('GET', 'localhost:3000/checkAndCrawl', [
				'query' => ['asin' => $asin]
			]);
			// $client = new Client([]);
			// $request = new Request('GET', "http://localhost:3000/checkAndCrawl?asin=$asin");
			// $res = $client->sendAsync($request);
			// $res->then(
			// 	function () {
			// 	},
			// 	function () {
			// 	}
			// );
		} catch (\Throwable $th) {
		}
	}

	public function crawlByProxyCrawl($conditions)
	{
		// use for crawl variant exist on db
		$amzUrl = urlencode($conditions['url']);
		$url = "https://api.proxycrawl.com/scraper?token=PIkPCxbHk-o1WcifgN8rOw&url=$amzUrl";

		$ch = curl_init();
		curl_setopt($ch, CURLOPT_URL, $url);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);

		$response = curl_exec($ch);
		curl_close($ch);

		return $response;
	}

	public function parseProxyCrawl($response)
	{
		// use for not exist in db
		$response = json_decode($response, true);
		if (empty($response) || empty($response['body']) || $response['pc_status'] != 200) return;

		$data = $response['body'];

		$parsedData = [
			'name' 			=> $data['name'],
			'price'			=> $data['price'],
			'isPrime'		=> $data['isPrime'] ?? null,
			'main_image'	=> $data['mainImage'],
			'images'		=> is_array($data['images']) ? json_encode($data['images']) : null,
			'features'		=> is_array($data['features']) ? json_encode($data['features']) : null,
			'description'	=> is_array($data['description']) ? json_encode($data['description']) : null,
			'time'			=> mdate('%Y-%m-%d %h:%i', now('asia/bangkok')),
			'weight'		=> 0
		];

		foreach ($data['productInformation'] as $info) {
			switch (strtolower($info['name'])) {
				case 'package dimensions':
					$matchesOunces = [];
					$matchesPounds = [];
					preg_match("/(\d+(\.\d{0,})?|\.?\d{1,}) ounces/i", strtolower($info['value']), $matchesOunces);
					preg_match("/(\d+(\.\d{0,})?|\.?\d{1,}) pounds/i", strtolower($info['value']), $matchesPounds);
					$parsedData['weight'] = !empty($matchesOunces[1]) ? $matchesOunces[1] * 0.0283 : $parsedData['weight'];
					$parsedData['weight'] = !empty($matchesPounds[1]) ? $matchesPounds[1] * 0.4535 : $parsedData['weight'];
					break;

				case 'product dimensions':
					$matchesOunces = [];
					$matchesPounds = [];
					preg_match("/(\d+(\.\d{0,})?|\.?\d{1,}) ounces/i", strtolower($info['value']), $matchesOunces);
					preg_match("/(\d+(\.\d{0,})?|\.?\d{1,}) pounds/i", strtolower($info['value']), $matchesPounds);
					$parsedData['weight'] = !empty($matchesOunces[1]) ? $matchesOunces[1] * 0.0283 : $parsedData['weight'];
					$parsedData['weight'] = !empty($matchesPounds[1]) ? $matchesPounds[1] * 0.4535 : $parsedData['weight'];
					break;

				case 'item weight':
					$matchesOunces = [];
					$matchesPounds = [];
					preg_match("/(\d+(\.\d{0,})?|\.?\d{1,}) ounces/i", strtolower($info['value']), $matchesOunces);
					preg_match("/(\d+(\.\d{0,})?|\.?\d{1,}) pounds/i", strtolower($info['value']), $matchesPounds);
					$parsedData['weight'] = !empty($matchesOunces[1]) ? $matchesOunces[1] * 0.0283 : $parsedData['weight'];
					$parsedData['weight'] = !empty($matchesPounds[1]) ? $matchesPounds[1] * 0.4535 : $parsedData['weight'];
					break;

				case 'asin':
					$parsedData['asin'] = $info['value'];
					break;
			}
		}

		return $parsedData;
	}

	public function crawlByAmzProductApi($asin)
	{
		$curl = curl_init();
		curl_setopt_array($curl, array(
			CURLOPT_URL => "https://amazon-product-reviews-keywords.p.rapidapi.com/product/details?country=US&asin=$asin",
			CURLOPT_RETURNTRANSFER => true,
			CURLOPT_FOLLOWLOCATION => true,
			CURLOPT_ENCODING => "",
			CURLOPT_MAXREDIRS => 10,
			CURLOPT_TIMEOUT => 30,
			CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
			CURLOPT_CUSTOMREQUEST => "GET",
			CURLOPT_HTTPHEADER => array(
				"x-rapidapi-host: amazon-product-reviews-keywords.p.rapidapi.com",
				"x-rapidapi-key: 479d9f2f88msh9b07230f6d25980p1c754djsn062d7ce368fb"
			),
		));

		$response = curl_exec($curl);
		// $err = curl_error($curl);
		curl_close($curl);

		return $response;
	}

	// crawl and save to db variants for first time crawl
	public function processFirstCrawl($crawledData)
	{
		// parse before process
		$parsedProduct = $this->parseAmzProductApi($crawledData);

		$requestVariantAsins = array_map(function ($var) {
			return $var['asin'];
		}, $parsedProduct['variants']);

		// Check whether group of product exist
		$groupProductRepresentAsin = $this->db->from('amz_product_groups')
			->where_in('asin', $requestVariantAsins)->get()
			->row()->represent_asin ?? $parsedProduct['asin'];

		$existVariantAsins = $this->db->from('amz_products')->select('asin')
			->where_in('asin', $requestVariantAsins)->get()->result() ?? [];
		// $existVariantAsins = array_map(function ($variant) { return $variant['asin']; }, $existVariants);
		$notExistVarients = [];

		// add more asin to group if exist and new group if not exist
		foreach ($parsedProduct['variants'] as $variant) {
			// add new asin to group
			$this->db->replace('amz_product_groups', ['asin' => $variant['asin'], 'represent_asin' => $groupProductRepresentAsin]);

			if (!in_array($variant['asin'], $existVariantAsins)) { // if new variant asin not exist then add to group
				// ['asin', 'variant_name', 'main_image']
				$notExistVarients[] = $variant;
			}
		}
		try {
			$this->db->insert_batch('amz_products', $notExistVarients);
			return true;
		} catch (\Throwable $th) {
			var_export($th);
			die;
		}
	}

	public function parseAmzProductApi($data)
	{
		// use for not exist in db
		$parsedData = ['asin' => $data['asin']];

		$parsedData['variants'] = array_map(function ($var) {
			return [
				'asin' => $var['asin'],
				'main_image' => $var['images'][0] ? ($var['images'][0]['large'] ?? $var['images'][0]['hiRes'] ?? null) : null,
				'variant_name' => $var['title']
			];
		}, $data['variants']);

		return $parsedData;
	}
}
