<?php
defined('BASEPATH') or exit('No direct script access allowed');
?>
<!DOCTYPE html>
<html lang="en">

<head>
	<meta charset="utf-8">
	<title>Welcome to CodeIgniter</title>

	<style type="text/css">
		::selection {
			background-color: #E13300;
			color: white;
		}

		::-moz-selection {
			background-color: #E13300;
			color: white;
		}

		body {
			margin: auto;
			background-color: #fff;
			font: 13px/20px normal Helvetica, Arial, sans-serif;
			color: #4F5155;
		}

		.spinner-container {
			width: 100%;
			height: 100%;
			text-align: center;
			position: fixed;
			top: 0;
			left: 0;
			z-index: 999;
		}

		.spinner-layer {
			width: 100%;
			height: 100%;
			background: black;
			opacity: .5;
			position: fixed;
			top: 0;
			left: 0;
			z-index: 998;
		}

		.spinner {
			position: fixed;
			z-index: 999;
			top: 45%;
			left: 45%;
			width: 100px !important;
			height: 100px !important;
		}
	</style>
	<script src="https://cdn.jsdelivr.net/npm/vue/dist/vue.js"></script>
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css">
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
	<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.16.0/umd/popper.min.js"></script>
	<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.5.2/js/bootstrap.min.js"></script>
	<script src="https://cdnjs.cloudflare.com/ajax/libs/lodash.js/4.17.20/lodash.min.js" integrity="sha512-90vH1Z83AJY9DmlWa8WkjkV79yfS2n2Oxhsi2dZbIv0nC4E6m5AbH8Nh156kkM7JePmqD6tcZsfad1ueoaovww==" crossorigin="anonymous"></script>
	<script src="https://cdnjs.cloudflare.com/ajax/libs/axios/0.21.0/axios.min.js" integrity="sha512-DZqqY3PiOvTP9HkjIWgjO6ouCbq+dxqWoJZ/Q+zPYNHmlnI2dQnbJ5bxAHpAMw+LXRm4D72EIRXzvcHQtE8/VQ==" crossorigin="anonymous"></script>
</head>

<body>
	<div id="spinner" class="spinner-container" style="display: none;">
		<div class="spinner-layer"></div>
		<div class="spinner-border text-info spinner" role="status"></div>
	</div>
	<div class="row container mt-3">
		<label for="amz-url">Link amz</label>
		<input required id="amz-url" type="text" name="url">
		<button type="button" id="btnSend">Search</button>
	</div>
	<div class="container-fluid mt-2" style="margin-bottom: 200px;" id="container">
		<?php if ($product) { ?>
			<div class="container">
				<div v-if="showSpinner" class="spinner-container">
					<div class="spinner-layer"></div>
					<div class="spinner-border text-info spinner" role="status"></div>
				</div>
				<div class="row">
					<div class="col-6 imgs" style="border: 1px solid black">
						<div class="col-12 mb-3"><img style="max-width: 90%;" :src="productDetail.main_image" /></div>
						<div class="row images">
							<div class="col-2" v-for="img in productDetail.images">
								<img style="width: 50px; height: 50px" :src="img" alt="">
							</div>
						</div>
					</div>
					<div class="col-6">
						<div class="col-12">Asin: {{productDetail.asin}}</div>
						<div class="col-12">Name: {{productDetail.name}}</div>
						<div class="col-12">Price: {{productDetail.symbol}}{{productDetail.price}} </div>
						<div class="col-12" v-if="productDetail.sale_price > 0">Sale Price: {{productDetail.symbol}}{{productDetail.sale_price}} </div>
						<div class="col-12">Shipping Price: {{productDetail.symbol}}{{productDetail.shipping_price}}</div>
						<div class="col-12">Shipping Detail: {{productDetail.shipping_detail}}</div>
						<div class="col-12">Weight: {{productDetail.weight}} kg</div>
						<div class="col-12">Availability: {{productDetail.in_stock}}</div>
						<div v-if="productDetail.category" class="ml-3">Category: {{ productDetail.category.join(' / ') }}</div>
						<div class="row variants">
							<div class="col-12 my-2 ml-3" v-for="variantObj in productDetail.variants">
								<button type="button" class="btn btn-outline-secondary ml-1" v-for="variant in variantObj.values" v-if="variant.available" v-bind:class="{ active: variant.selected || productDetail.asin == variant.asin || variant.dpUrl.includes(productDetail.asin) }" @click="changeVariant(variant.asin || variant.dpUrl)">
									<img v-if="variant.imageUrl" v-bind:src="variant.imageUrl" v-bind:alt="variant.value">
									{{ variant.imageUrl == null ? variant.value : "" }}
								</button>
							</div>
						</div>
						<h2>Features:</h2>
						<ul class="w-100 features">
							<li class="col-12" v-for="feature in productDetail.features">{{feature}}</li>
					</div>
				</div>

				<div class="row description">
					<h3>Product description</h3>
					{{productDetail.description}}
				</div>
			</div>
	</div>
<?php } else { ?>
	<div>Không tim thấy sản phẩm</div>
<?php } ?>
</div>
<script>
	new Vue({
		el: '#container',
		data: {
			productDetail: <?= $product ?? "''" ?>,
			showSpinner: false,
			getVariantsInterval: null
		},
		methods: {
			init() {
				// get variants every 2s until have result
				// this.getVariantsInterval = setInterval(this.getVariants, 2000);
				this.parseProduct(this.productDetail);
				// this.checkSelectVariant(this.productDetail.asin);
			},
			getDefaultProductDetail() {
				return {
					asin: 'Not found',
					category: null,
					description: '',
					features: [],
					images: [],
					in_stock: false,
					main_image: '',
					name: 'Not found',
					price: 0,
					weight: 0
				}
			},
			checkSelectVariant(selectAsin) {
				_.map(this.productDetail.variants, variant => {
					variant.selected = variant.asin == selectAsin ? true : false;
				});
			},
			getVariants() {
				axios.get(`/variants/${this.productDetail.asin}`).then(res => {
					if (!res.data || !res.data.success) throw ('not have variants');
					_.map(res.data.data, variant => variant.selectd = false);
					this.productDetail.variants = res.data.data;
					this.checkSelectVariant(this.productDetail.asin);
					clearInterval(this.getVariantsInterval);
					this.$nextTick(function() {
						$('[data-toggle="tooltip"]').tooltip();
					})
				}).catch(err => {
					console.log(err);
				})
			},

			changeVariant(asin) {
				this.showSpinner = true;
				let url = `https://www.amazon.com/dp/${asin}?psc=1`;
				let encoded = encodeURIComponent(url);
				window.location.href = `/product?url=${encoded}`;
			},

			// changeVariant(asin) {
			// 	this.showSpinner = true;
			// 	axios.get(`/variant/${asin}`).then(res => {
			// 		if (!res.data || !res.data.success) throw ('not success');

			// 		this.parseProduct(res.data.data);
			// 		_.merge(this.productDetail, res.data.data);
			// 		this.checkSelectVariant(asin);
			// 		this.showSpinner = false;
			// 	}).catch(err => {
			// 		console.log(err);
			// 		_.merge(this.productDetail, this.getDefaultProductDetail());
			// 		this.showSpinner = false;
			// 	})
			// },

			parseProduct(productDetail) {
				try {
					productDetail.features = JSON.parse(productDetail.features);
				} catch (error) {
					productDetail.features = [];
				}

				try {
					productDetail.images = JSON.parse(productDetail.images);
				} catch (error) {
					productDetail.images = [];
				}

				try {
					productDetail.category = JSON.parse(productDetail.category);
				} catch (error) {
					productDetail.category = null;
				}

				try {
					productDetail.variants = JSON.parse(productDetail.variants);
				} catch (error) {
					productDetail.variants = null;
				}
			}
		},
		created() {
			this.init();
			window.app = this;
		}
	})
</script>
<script>
	$(document).ready(() => {
		$('#btnSend').click(() => {
			$('#spinner').show();
			let val = $('#amz-url').val();
			let encoded = encodeURIComponent(val);
			window.location.href = `/product?url=${encoded}`;
		});
	});
</script>
</body>

</html>